package com.qa.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.BaseClass;

public class LoginPage extends BaseClass {

	@FindBy(name = "uid")
	WebElement username;
	
	@FindBy(name = "password")
	WebElement password;
	
	@FindBy(name = "btnLogin")
	WebElement loginBtn;
	
	@FindBy(name = "btnReset")
	WebElement resetBtn;
	
	//Initializing Page
	
	public LoginPage() {
		PageFactory.initElements(driver,this);
	}
	
	public HomePage	login() {
		username.sendKeys("mngr275213");
		password.sendKeys("vEsyzyd");
		
		return new HomePage();
	}
	
		
	public void loginButton() {
		//JavascriptExecutor js =(JavascriptExecutor) driver;
		//js.executeScript("argument[0].click();", loginBtn);
		//loginBtn.click();
		 driver.findElement(By.name("btnLogin")).click();
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		
	}
	public void verifyReset() {
		try {
		if(username.getText().contains("") == true) {
			 System.out.println("reset successfull");
		}
		}catch(Exception e) {
		}
		
	}
	public void resetButton() {
		resetBtn.click();
	}
		
		
	}
	
	

