Feature: feature to check new account functionality

  Background: Given user enters username password and clicks on login buton
    Then user is on home page

  Scenario: Check new account functionality
    When user clicks on new account tap
    And user is navigated to new account and enters invalid initial deposit
    Then displayed error message
