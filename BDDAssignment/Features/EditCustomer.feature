Feature: Feature to check edit functionality

  Scenario: Check customer info edited succesfully
    Given User clicks on Edit Customer tab
    When user enters customer id
    And clicks on submit button
    And user is navigated to edit page and edits address and clicks on submit
    Then customer details edited successfuly page is displayed
