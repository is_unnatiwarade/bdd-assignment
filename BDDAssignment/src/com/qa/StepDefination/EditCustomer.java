package com.qa.StepDefination;

import org.openqa.selenium.WebDriver;
import com.qa.pages.EditByCustIdPage;
import com.qa.pages.EditCust;
import com.qa.pages.HomePage;
import com.qa.pages.LoginPage;
import com.qa.pages.UpdatedPage;
import com.qa.util.BaseClass;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;

public class EditCustomer extends BaseClass {
	LoginPage loginPage;
	HomePage homePage;
	EditByCustIdPage editCustIdPage;
/*
	@Before
	public void launch() {
		BaseClass.initialize();
	}

	@After
	public void close() {
		BaseClass.closeDriver();
	}
*/
	@Given("User clicks on Edit Customer tab")
	public void user_clicks_on_Edit_Customer_tab() {
		loginPage = new LoginPage();
		homePage = new HomePage();
		homePage = loginPage.login();
		loginPage.loginButton();
		homePage.clickOnEditCustomer();
		editCustIdPage = new EditByCustIdPage();

	}

	@When("user enters customer id")
	public void user_enters_customer_id() {
		editCustIdPage.cusId();
	}

	@And("clicks on submit button")
	public void clicks_on_submit_button() {
		// driver.findElement(By.xpath("/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[11]/td[2]/input[1]")).click();
		editCustIdPage.submitbtnClick();
		// driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
	}

	@And("user is navigated to edit page and edits address and clicks on submit")
	public void user_edits_address_and_clicks_on_submit() {
		EditCust edit = new EditCust();
		edit.editAddress();
		edit.clickResetSubmit();
	}

	@Then("customer details edited successfuly page is displayed")
	public void customer_details_edited_successfuly_page_is_displayed() {
		UpdatedPage updatedPage = new UpdatedPage();
		updatedPage.verifyUpdatedDetailsTitle();
		updatedPage.verifyUpdatedAddress();
	}
}
