package com.qa.StepDefination;

import org.openqa.selenium.WebDriver;

import com.qa.pages.HomePage;
import com.qa.pages.LoginPage;
import com.qa.util.BaseClass;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;

public class guruLogin extends BaseClass {
	LoginPage loginPage;
	HomePage homePage;
	WebDriver driver = null;
/*
	@Before
	public void launch() {
		BaseClass.initialize();
	}

	@After
	public void close() {
		BaseClass.closeDriver();
	}
*/
	@Given("User enters username and password")
	public void user_enters_username_and_password() {
		// driver.findElement(By.name("uid")).sendKeys("mngr275213");
		// driver.findElement(By.name("password")).sendKeys("vEsyzyd");
		loginPage = new LoginPage();
		homePage = new HomePage();
		homePage = loginPage.login();
	}

	@When("clicks on login button")
	public void clicks_on_login_button() {
		loginPage.loginButton();
	}

	@Then("User is navigated to home page")
	public void user_is_navigated_to_home_page() {
		// String homePageTitle =
		homePage.verifyHomePageTitle();
		// Assert.assertEquals("Welcome To Manager's Page of Guru99 Bank",
		// homePageTitle);

	}

}
