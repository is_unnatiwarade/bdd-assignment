package com.qa.StepDefination;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.pages.HomePage;
import com.qa.pages.LoginPage;
import com.qa.pages.NewAccountPage;
import com.qa.util.BaseClass;
import com.qa.util.TestUtil;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;

public class NewAccount extends BaseClass {

	LoginPage loginPage;
	HomePage homePage;
	NewAccountPage newAcc;

	/*@Before
	public void launch() {
		BaseClass.initialize();
		loginPage = new LoginPage();
		homePage = new HomePage();
		homePage = loginPage.login();
		loginPage.loginButton();
	}

	@After
	public void close() {
		BaseClass.closeDriver();
	}
	*/

	@When("user clicks on new account tap")
	public void user_clicks_on_new_account_tap() {
		//homePage.clickOnNewCustomer();

		driver.findElement(By.xpath("//a[contains(text(),'New Account')]")).click();
	
		
		// driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		// driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

	}

	@And("user is navigated to new account and enters invalid initial deposit")
	public void user_is_navigated_to_new_account_and_enters_invalid_initial_deposit() {
			newAcc = new NewAccountPage();
			//newAcc.verifyNewAccTittle();
			// newAcc = new NewAccountPage();
			//newAcc.invalidCustId();
			try {
			 driver.findElement(By.name("cusid")).sendKeys("!@#$");
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
		
		
	}

	@Then("displayed error message")
	public void displayed_error_message() {
		try {
			//newAcc = new NewAccountPage();
			//newAcc.verifyNewAccTittle();
			//driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);	
			newAcc.verifySpecialCharErrorMsg();
		} catch (Exception e) {
			e.printStackTrace();
		}
		BaseClass.closeDriver();
	}

}
