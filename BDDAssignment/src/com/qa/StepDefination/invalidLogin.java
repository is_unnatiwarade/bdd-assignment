package com.qa.StepDefination;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.qa.pages.HomePage;
import com.qa.pages.LoginPage;
import com.qa.util.BaseClass;

import io.cucumber.java.en.*;

public class invalidLogin extends BaseClass{
	WebDriver driver = null;
	
	@Given("user is on the login page")
	public void user_is_on_the_login_page() {
		BaseClass.initialize();
	}

	//@When("user enters invalid \"([^\"]*)\" and \"([^\"]*)\"")
	@When("user enters invalid abc and acgfj")
	public void user_enters_invalid_and_(String userName, String password) {
		driver.findElement(By.name("uid")).sendKeys(userName);
		driver.findElement(By.name("password")).sendKeys(password);
		
	}

	@And("user hits login button")
	public void user_hits_login_button() {
		LoginPage loginPage = new LoginPage();
		loginPage.loginButton();
	}

	@Then("login should be unsuccessful")
	public void login_should_be_unsuccessful() {
	    driver.close();
	}



}
