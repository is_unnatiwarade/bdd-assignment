package com.qa.StepDefination;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.pages.HomePage;
import com.qa.pages.LoginPage;
import com.qa.util.BaseClass;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AddNewCustomerStep extends BaseClass {
	LoginPage loginPage;
	HomePage homePage;

	/*
	 * @Given("user is on home page") public void user_is_on_home_page() {
	 * BaseClass.initialize(); loginPage = new LoginPage(); homePage = new
	 * HomePage(); homePage = loginPage.login(); loginPage.loginButton(); //
	 * driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
	 * 
	 * }
	 */

	@When("user clicks on New Customer tap")
	public void user_clicks_on_New_Customer_tap() {
		WebDriverWait wait=new WebDriverWait(driver, 20);
		driver.findElement(By.xpath("//a[contains(text(),'New Customer')]")).click();
		//homePage.clickOnNewCustomer();

	}

	@And("user enters number in customer name")
	public void user_enters_number_in_customer_name() {
		driver.findElement(By.name("name")).sendKeys("12345");
		//homePage.invalidCustomerNameTextBox();
		
	}

	@Then("Numbers are not allowed message is displayed")
	public void Numbers_are_not_allowed_message_is_displayed() {
		try {
		String errorMsg = homePage.invalidCustName();
		if (errorMsg.contentEquals("Numbers are not allowed") == true) {
			System.out.println("Numbers are not allowed");
		} else {
			System.out.println("Error not displayed");
		}
		}catch (Exception e) {
			// TODO: handle exception
		}
		driver.close();
	}
}
