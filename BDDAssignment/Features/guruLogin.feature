#Author
#Date
#Description
Feature: verify login functionality

  Scenario: verify succsessful login with valid credentials
    Given User enters username and password
    When clicks on login button
    Then User is navigated to home page

  @SmokeTest @RegressionTest
  Scenario: veriy by clicking on reset button username and password text should to blank
    Given User is on the guru login page
    When User enters userid and password
    And Clicks on reset button
    Then userid and password should be blank

  Scenario Outline: Check login with invalid credentials generates error
    Given user is on the login page
    When user enters invalid <username> and <password>
    And user hits login button
    Then login should be unsuccessful

    Examples: 
      | username | password |
      | abc      | acgfj    |
