package com.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.util.BaseClass;

public class HomePage extends BaseClass {

	@FindBy(xpath = "//marquee[contains(text(),'Guru99')]")
	WebElement homePageTitle;

	@FindBy(xpath = "//a[contains(text(),'Manager')]")
	WebElement managerTab;

	@FindBy(xpath = "//a[contains(text(),'New Customer')]")
	WebElement newCustomer;

	@FindBy(name = "name")
	WebElement custNameTextBox;

	@FindBy(xpath = "//a[contains(text(),'Edit Customer')]")
	WebElement editCustomer;

	@FindBy(id = "message")
	WebElement invalidCustNameError;

	@FindBy(xpath = "//a[contains(text(),'New Account')]")
	WebElement newAccount;

	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	public String verifyHomePageTitle() {
		return homePageTitle.getText();
	}

	public void clickOnNewCustomer() {
		newCustomer.click();
	
	}

	public void clickOnEditCustomer() {
		editCustomer.click();
	}

	public void clickOnAddAccount() {
		newAccount.click();
	}

	public void invalidCustomerNameTextBox() {
		try {
			WebDriverWait wait=new WebDriverWait(driver,20);
				custNameTextBox.click();
				custNameTextBox.sendKeys("123456");
			
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String invalidCustName() {
		return invalidCustNameError.getText();
	}

}
