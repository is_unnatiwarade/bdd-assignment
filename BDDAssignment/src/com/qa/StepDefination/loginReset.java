package com.qa.StepDefination;

import org.openqa.selenium.WebDriver;
import com.qa.pages.HomePage;
import com.qa.pages.LoginPage;
import com.qa.util.BaseClass;
import io.cucumber.java.en.*;


public class loginReset extends BaseClass {
	LoginPage loginPage;
	HomePage homePage;
	WebDriver driver = null;
	
	@Given("User is on the guru login page")
	public void user_is_on_the_guru_login_page() {
		BaseClass.initialize();
	}

	@When("User enters userid and password")
	public void user_enters_userid_and_password() {
		loginPage = new LoginPage();
		homePage = new HomePage();
		homePage = loginPage.login();
	}

	@When("Clicks on reset button")
	public void clicks_on_reset_button() {
	   loginPage.resetButton();
	}

	@Then("userid and password should be blank")
	public void userid_and_password_should_be_blank() {
	    loginPage.verifyReset();
	}



}
