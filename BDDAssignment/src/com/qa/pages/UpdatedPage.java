package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class UpdatedPage {

	@FindBy(className = "header3")
	WebElement editedSuccess;

	@FindBy(xpath = "//*[@id=\"customer\"]/tbody/tr[8]/td[2]")
	WebElement updatedAddress;

	public void verifyUpdatedDetailsTitle() {
		try {
		if (editedSuccess.isDisplayed() == true) {
			System.out.println("Customer details updated successfully");
		} else {
			System.out.println("Customer details not updated");
		}
	}catch(Exception e) {
		e.printStackTrace();
	}
	}

	public void verifyUpdatedAddress() {
		try {
		 updatedAddress.getText().contains("xyz");
		}catch(Exception e) {
			e.printStackTrace();
	}
}
}
