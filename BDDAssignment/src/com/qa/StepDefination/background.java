package com.qa.StepDefination;

import org.openqa.selenium.WebDriver;

import com.qa.pages.HomePage;
import com.qa.pages.LoginPage;
import com.qa.util.BaseClass;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class background extends BaseClass {
	LoginPage loginPage;
	HomePage homePage;

	@Given("user enters username password and clicks on login buton")
	public void user_is_on_login_page() {
		BaseClass.initialize();
		loginPage = new LoginPage();
		homePage = new HomePage();
		homePage = loginPage.login();
		loginPage.loginButton();

	}

	@Then("user is on home page")
	public void clicks_on_login_button() {

	}

}
