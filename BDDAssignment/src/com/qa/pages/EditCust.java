package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EditCust {

	@FindBy(name = "addr")
	WebElement addressBox;
	@FindBy(name = "sub")
	WebElement resetSubmitButton;
	
	public void editAddress() {
		try {
		addressBox.sendKeys("xyz");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void clickResetSubmit() {
		try {
		resetSubmitButton.click();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
