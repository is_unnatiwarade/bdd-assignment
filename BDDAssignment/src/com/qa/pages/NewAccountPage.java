package com.qa.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.qa.util.BaseClass;

public class NewAccountPage extends BaseClass {

	@FindBy(className = "heading3")
	WebElement newAccTittle;

	@FindBy(xpath = "/html/body/table/tbody/tr/td/table/tbody/tr[2]/td[2]/input")
	WebElement custIdTextbx;

	@FindBy(id = "message14")
	WebElement specialCharError;

	public void verifyNewAccTittle() {
		if (newAccTittle.getText().contains("Add new account form") == true) {
			System.out.println("Navigated");
		} else {
			System.out.println("Not Navigated");
		}

	}

	public void invalidCustId() {
		
		custIdTextbx.sendKeys("!@#$");
		
	}

	public void verifySpecialCharErrorMsg() {
		
			if (specialCharError.getText().contains("Special characters are not allowed") == true) {
				System.out.println("Error Msg displayed successfully");
			}
		
	}
}
