package com.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.qa.util.BaseClass;

public class EditByCustIdPage extends BaseClass{
	@FindBy(className = "heading3")
	WebElement editPageHeader;
	
	@FindBy(name = "cusid")
	WebElement custIdTextBox;
	
	@FindBy(xpath = "/html/body/div[2]/table/tbody/tr/td/table/tbody/tr[11]/td[2]/input[1]")
	WebElement submitButton;
	
	public String verifyEditCustPageHeader() {
		return editPageHeader.getText();
	}
	public void cusId() {
		//custIdTextBox.sendKeys("39037");
		 driver.findElement(By.name("cusid")).sendKeys("39037");
	}
	public void submitbtnClick() {
		try {
		submitButton.click();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
